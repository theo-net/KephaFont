'use strict' ;

/**
 * ./build.js
 */

// Outils pour la génération de la police
const svgicons2svgfont = require('svgicons2svgfont'),
      svg2ttf          = require('svg2ttf'),
      ttf2eot          = require('ttf2eot'),
      ttf2woff         = require('ttf2woff'),
      ttf2woff2        = require('ttf2woff2'),
      fs               = require('fs'),
      cli              = require('kephajs').cli(),
      color            = cli.color() ;

// Outils de KephaFont
const utils = require('./utils') ;

// Version de la police
const pkg     = require('./package.json'),
      version = pkg.version ;


/**
 * Créé les différents formats de polices
 */
function convertFont (config, callback) {

  // TTF
  let ttf = svg2ttf(fs.readFileSync(
                config.pathFont + '/' + config.fontName + '.svg')
              .toString(), {}) ;
  fs.writeFileSync(
      config.pathFont + '/' + config.fontName + '.ttf',
      new Buffer(ttf.buffer)
  ) ;
  console.log(color.blue('TTF font created!')) ;

  // EOT
  let eot = new Buffer(ttf2eot(ttf.buffer).buffer) ;
  fs.writeFile(
      config.pathFont + '/' + config.fontName + '.eot',
      eot, function (err) {
        if (err) throw err ;
        console.log(color.blue('EOT font created')) ;
      }) ;

  // WOFF
  let woff = new Buffer(ttf2woff(ttf.buffer).buffer) ;
  fs.writeFile(
      config.pathFont + '/' + config.fontName + '.woff',
      woff, function (err) {
        if (err) throw err ;
        console.log(color.blue('WOFF font created')) ;
      }) ;

  // WOFF2
  let woff2 = new Buffer(ttf2woff2(ttf.buffer).buffer) ;
  fs.writeFile(
      config.pathFont + '/' + config.fontName + '.woff2',
      woff2, function (err) {
        if (err) throw err ;
        console.log(color.blue('WOFF2 font created')) ;
        callback() ;
      }) ;
}


/**
 * Génère la police
 */
function generate (config) {

  // Initialisation de la police
  let fontStream = new svgicons2svgfont({
    fontName: config.fontName,
    normalize: true,
    metadata: config.author + '. ' + config.license
  }) ;
  fontStream.pipe(
    fs.createWriteStream(config.pathFont + '/' + config.fontName + '.svg')
  ).on('finish', function () {

    console.log(color.blue('SVG font successfully created!')) ;

    utils.createLess(glyphs, config) ;
    convertFont(config, () => {
      console.log(color.bold.green('Font ' + config.fontName
                                 + ' successfully created!')) ;
    }) ;
  }).on('error', function (err) {
      console.log(color.red(err)) ;
  }) ;

  // Chargement des glyphs
  let unicode = Number(config.firstGlyph) ;
  let glyphs = Array() ;
  let glyphsList = utils.loadGlyphsList(config) ;
  glyphsList.forEach(function (glyphName) {

    let glyph = fs.createReadStream(config.pathSvg + '/' + glyphName + '.svg') ;
    glyph.metadata = {
      unicode: [String.fromCharCode(unicode)],
      name: glyphName
    } ;
    glyph.metadata.string = '\\' + unicode.toString(16).toUpperCase() ;
    glyphs.push(glyph.metadata) ;
    unicode++ ;

    fontStream.write(glyph) ;
  }) ;
  fontStream.end() ;
}


/**
 * Le programme
 */
cli.version(version).action(() => {

  console.log(color.green.bold('Génération de la police...')) ;

  // Initialisation du répertoire
  cli.K.rmrf('./dist', () => {
    fs.mkdir('./dist', () => {
      fs.mkdir('./dist/font', () => {
        console.log(color.yellow('répertoire de destination vidé')) ;

        let config = require('./config') ;
        generate(config) ;

        // Création d'un fichier contenant la liste des glyphes
        let glyphes = utils.loadGlyphsList(config, true) ;
        fs.writeFile(config.pathList, JSON.stringify(glyphes), (err) => {
          if (err) throw new Error ('Write list gyphe file error :' + err) ;
          console.log(color.green('Fichier list créé')) ;
        }) ;
      }) ;
    }) ;
  }) ;
}) ;

cli.run() ;

