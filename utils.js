'use strict' ;

/**
 * ./utils.js
 */

let fs = require('fs') ;

/**
 * Créé le fichier Less
 */
let createLess = function (glyphs, config) {

  // Charge le template
  fs.readFile(config.pathLessSrc, function (err, data) {

    if (err) throw new Error ('Loading dist less file error :' + err) ;

    data = data.toString().replace(/{{fontName}}/g, config.fontName) ; 

    // liste des caractères
    let listChar = '' ; 
    // les classes
    let classes = '' ; 

    // On récupère les glyphes colorés
    let color = config.color ;

    // On prépare le contenu du Less
    glyphs.forEach(function (glyph) { 

      listChar += '  @' + glyph.name + ': "' 
               + glyph.string + '" ; \n' ;
      classes  += '.icon-' + glyph.name + ' { \n' 
               +  '  .icon(' + glyph.name + ') ; \n'
               +  '} \n' ; 
      if (color[glyph.name] !== undefined) {
        classes  += '.icon-' + glyph.name + '-color { \n' 
                 +  '  .icon(' + glyph.name + ') ; \n'
                 +  '  color: ' + color[glyph.name] + ' ;\n'
                 +  '} \n' ; 
      }
    }) ; 

    // On insert la liste des caractères et des classes
    data = data.toString().replace(/{{listChar}}/, listChar) ; 
    data = data.toString().replace(/{{classes}}/, classes) ; 

    // On sauve le fichier
    fs.writeFile(config.pathLess, data, (err) => {
      if (err) throw new Error ('Write dist less file error :' + err) ;
      console.log('Fichier LESS créé') ; 
    }) ; 
  }) ; 
}


/**
 * Charge les glyphs
 * @param {Boolean} [color=false] Récupère-t-on les versions colorées ?
 * @returns [Array]
 */
let loadGlyphsList = function (config, color = false) {
 
  let glyphs = Array() ; 
 
  let files = fs.readdirSync(config.pathSvg) ;
  
  files.forEach(function (file) {

    if (file.search(/\.svg$/) > -1) {
      glyphs.push(file.match(/^(.+)\.svg$/)[1]) ; 
    }
  }) ; 

  if (color) {
    Object.keys(config.color).forEach(glyph => {
      glyphs.push(glyph + '-color') ;
    }) ;
  }
 
  return glyphs ; 
} ; 

// On exporte les fonctions
exports.createLess = createLess ;
exports.loadGlyphsList = loadGlyphsList ;

