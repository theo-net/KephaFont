'use strict' ;

/**
 * ./test.js
 */

const fs    = require('fs'),
      copy  = require('cp-r'),
      less  = require('less'),
      cli   = require('kephajs').cli(),
      color = cli.color() ;
const config = require('./config'),
      utils  = require('./utils') ;

const version = process.env.npm_package_version ;


cli.version(version).action(() => {

  // On copie les polices
  copy('dist/font', 'test') ;

  // On transforme le LESS en CSS
  fs.readFile('dist/KephaFont.less', 'utf8', (error, data) => {

    if (error) throw error ;

    // Rendu de la page
    less.render(data, (e, output) => {


      let  html = `<!DOCTYPE html>
<html>
  <head>
    <title>Test de KephaLess</title>
    <style>
      ` + output.css.replace(/\.\.\/fonts\/KephaFont\//g, '') + `
    </style>
  <head>

  <body>
    <h1>Tests <em>KephaLess</em> v.` + version + `</h1>
      <table>
        <tr>
          <th>Name</th>
          <th>Icon</th>
          <th>Pulse</th>
          <th>Spin</th>
        </tr>
` ;

      // On affiche tous les caractères
      utils.loadGlyphsList(config, true).forEach(glyph => {

        html += `<tr>
          <td>` + glyph + `</td>
          <td><i class="icon icon-` + glyph + ` icon-2x"></i></td>
          <td><i class="icon icon-` + glyph + ` icon-2x icon-pulse"></i></td>
          <td><i class="icon icon-` + glyph + ` icon-2x icon-spin"></i></td>
        </tr>` ;
      }) ;


      html += `
    </table>
  </body>
<html>
` ;

      // On écrit le fichier
      fs.writeFile('test/test.html', html, (err) => {
        if (err) throw err ;
        console.log(color.green(
          'Test generate: open test.html in your browser')) ;
      }) ;
    }) ;
  }) ;
}) ;
cli.run() ;

